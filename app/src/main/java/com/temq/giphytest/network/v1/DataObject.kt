package com.temq.giphytest.network.v1

import com.google.gson.annotations.SerializedName

data class DataObject<T>(@SerializedName("data") val data: T)
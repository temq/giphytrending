package com.temq.giphytest.network.v1

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class ImageTypeSrializer : JsonDeserializer<ImageTypeV1> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ImageTypeV1 {

        return json?.asString.let {
            when (it) {
                "gif" -> ImageTypeV1.GIF
                else -> ImageTypeV1.GIF
            }
        }

    }
}
package com.temq.giphytest.network.v1

import com.google.gson.annotations.SerializedName

data class PagingObject<T>(@SerializedName("data") val data: List<T>)
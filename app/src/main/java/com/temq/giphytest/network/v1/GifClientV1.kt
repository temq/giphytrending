package com.temq.giphytest.network.v1

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GifClientV1 {

    @GET("v1/gifs/trending")
    fun loadGifs(@Query("limit") limit: Int,
                 @Query("offset") offset: Int): Single<PagingObject<ImageInfoV1>>

    @GET("/v1/gifs/{gif_id}")
    fun findGif(@Path("gif_id") id: String): Single<DataObject<ImageInfoV1>>

}
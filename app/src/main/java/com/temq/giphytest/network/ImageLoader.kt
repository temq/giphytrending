package com.temq.giphytest.network

import com.temq.giphytest.model.ImageInfo
import io.reactivex.Single

interface ImageLoader {

    fun loadGifs(offset: Int, limit: Int): Single<List<ImageInfo>>

    fun loadGif(id: String): Single<ImageInfo>

}
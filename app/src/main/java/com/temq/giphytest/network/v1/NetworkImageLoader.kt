package com.temq.giphytest.network.v1

import com.temq.giphytest.model.Image
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.model.ImageType
import com.temq.giphytest.model.User
import com.temq.giphytest.network.ImageLoader
import io.reactivex.Single
import javax.inject.Inject

class NetworkImageLoader @Inject constructor(private val api: GifClientV1) : ImageLoader {
    override fun loadGifs(offset: Int, limit: Int): Single<List<ImageInfo>> {
        return api.loadGifs(limit, offset)
                .map { data ->
                    data.data.map { mapImageInfo(it) }
                }
    }

    override fun loadGif(id: String): Single<ImageInfo> = api.findGif(id)
            .map { mapImageInfo(it.data) }

    private fun mapImageInfo(infoV1: ImageInfoV1) = ImageInfo(
            typeV1 = mapType(infoV1.typeV1),
            id = infoV1.id,
            title = infoV1.title,
            userName = infoV1.userName,
            userV1 = mapUser(infoV1.userV1),
            images = mapImages(infoV1.images)
    )

    private fun mapType(type: ImageTypeV1) = ImageType.GIF

    private fun mapUser(userV1: UserV1?): User {
        return if (userV1 != null) {

            val twitterProfile = userV1.twitter.orEmpty()
            User(userV1.avatarUrl,
                    userV1.profileUrl,
                    userV1.userName,
                    userV1.displayName,
                    userV1.bannerUrl,
                    if (twitterProfile.startsWith("@"))
                        twitterProfile.substring(1)
                    else twitterProfile)
        } else {
            User()
        }
    }

    private fun mapImages(images: Map<String, ImageV1>) = images
            .map {
                val image = it.value.let { imageV1 ->
                    Image(imageV1.url.orEmpty(),
                            imageV1.width ?: 0,
                            imageV1.height ?: 0,
                            imageV1.size ?: 0)
                }
                return@map it.key to image
            }.toMap()
}
package com.temq.giphytest.network.v1

import com.google.gson.annotations.SerializedName

enum class ImageTypeV1 {
    GIF
}

data class UserV1(@SerializedName("avatar_url") val avatarUrl: String,
                  @SerializedName("profile_url") val profileUrl: String,
                  @SerializedName("username") val userName: String,
                  @SerializedName("display_name") val displayName: String,
                  @SerializedName("banner_url") val bannerUrl: String = "",
                  @SerializedName("twitter") val twitter: String?)

data class ImageV1(@SerializedName("url") val url: String?,
                   @SerializedName("width") val width: Int?,
                   @SerializedName("height") val height: Int?,
                   @SerializedName("size") val size: Int?)

data class ImageInfoV1(@SerializedName("type") val typeV1: ImageTypeV1,
                       @SerializedName("id") val id: String,
                       @SerializedName("title") val title: String,
                       @SerializedName("username") val userName: String,
                       @SerializedName("user") val userV1: UserV1?,
                       @SerializedName("images") val images: Map<String, ImageV1>)


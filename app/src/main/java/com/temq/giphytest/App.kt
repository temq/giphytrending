package com.temq.giphytest

import android.app.Application
import com.temq.giphytest.dagger.AppComponent
import com.temq.giphytest.dagger.DaggerAppComponent

class App: Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .context(this)
                .build()
    }

}
package com.temq.giphytest.ui.detail

import com.temq.giphytest.model.ImageInfo
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent
interface ImageDetailsComponent {

    fun presenter(): ImageDetailsPresenter

    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun imageInfo(imageInfo: ImageInfo?): Builder

        @BindsInstance
        fun imageId(id: String): Builder

        fun build(): ImageDetailsComponent
    }
}
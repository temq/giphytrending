package com.temq.giphytest.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class ProgressItemCreator : AdapterViewCreator<ProgressAdapterItem, ProgressItemCreator.ViewHolder> {
    override fun onCreateViewHolder(layoutInflater: LayoutInflater, viewGroup: ViewGroup): ViewHolder {
        val view = layoutInflater.inflate(ProgressAdapterItem.VIEW_TYPE, viewGroup, false)
        return ViewHolder(view)
    }

    override fun bind(viewHolder: ViewHolder, item: ProgressAdapterItem, context: Context, payloads: MutableList<Any>) {
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
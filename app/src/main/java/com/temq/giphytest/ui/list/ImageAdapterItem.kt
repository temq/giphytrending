package com.temq.giphytest.ui.list

import android.support.v7.widget.RecyclerView
import com.temq.giphytest.R
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.ui.adapter.AdapterItem

class ImageAdapterItem(private val item: ImageInfo) : AdapterItem<ImageInfo> {

    companion object {
        const val VIEW_TYPE = R.layout.item_image
    }

    override fun getViewType() = VIEW_TYPE

    override fun getId() = RecyclerView.NO_ID

    override fun getItem() = item

    override fun areItemSame(adapterItem: AdapterItem<Any>) = adapterItem is ImageAdapterItem
            && adapterItem.item.id == item.id

    //todo implement correct logic depending on fields which using for UI
    override fun areContentsSame(adapterItem: AdapterItem<Any>) = true
}
package com.temq.giphytest.ui.adapter

import android.content.Context
import android.support.v4.util.SparseArrayCompat
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

class AppRecyclerViewAdapter(private val context: Context,
                             private val inflater: LayoutInflater,
                             private val creators: SparseArrayCompat<AdapterViewCreator<AdapterItem<Any>, RecyclerView.ViewHolder>>)
    : ListAdapter<AdapterItem<Any>, RecyclerView.ViewHolder>(Callback()) {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
            creators[viewType]!!.onCreateViewHolder(inflater, viewGroup)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (!payloads.isEmpty()) {
            val item = getItem(position)
            val creator = creators[item.getViewType()]
            creator?.apply {
                bind(holder, item, context, payloads)
            }
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        val creator = creators[item.getViewType()]
        creator?.apply {
            bind(viewHolder, item, context)
        }
    }

    override fun getItemViewType(position: Int) = getItem(position).getViewType()

    override fun getItemId(position: Int) = getItem(position).getId()

    private class Callback : DiffUtil.ItemCallback<AdapterItem<Any>>() {
        override fun areItemsTheSame(p0: AdapterItem<Any>, p1: AdapterItem<Any>) = p0.areItemSame(p1)

        override fun areContentsTheSame(p0: AdapterItem<Any>, p1: AdapterItem<Any>) = p0.areContentsSame(p1)

        override fun getChangePayload(oldItem: AdapterItem<Any>, newItem: AdapterItem<Any>) = oldItem.getPayload(newItem)
    }
}
package com.temq.giphytest.ui.detail

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.temq.giphytest.ui.BaseView

interface ImageDetailsView : BaseView {
    fun setAuthorInfo(avatarUrl: String, userName: String, displayName: String)
    fun showTwitter(twitter: String)
    fun showGiphyProfile(show: Boolean)
    fun setImage(gifUrl: String)

    @StateStrategyType(SkipStrategy::class)
    fun openChrome(link: String)

    @StateStrategyType(SkipStrategy::class)
    fun openTwitter(userName: String)

    fun showTitle(title: String)
    fun close()
}
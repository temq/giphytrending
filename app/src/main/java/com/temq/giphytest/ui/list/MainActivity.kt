package com.temq.giphytest.ui.list

import android.os.Bundle
import android.support.v4.util.SparseArrayCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.temq.giphytest.R
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.ui.BaseActivity
import com.temq.giphytest.ui.adapter.*
import com.temq.giphytest.ui.detail.ImageDetailsActivity

class MainActivity : BaseActivity(), MainActivityView {

    companion object {
        const val COLUMNS = 2
    }

    @BindView(R.id.swipeRefresh)
    lateinit var refresh: SwipeRefreshLayout
    @BindView(R.id.items)
    lateinit var recyclerView: RecyclerView

    @InjectPresenter
    lateinit var presenter: MainPresenter

    lateinit var adapter: AppRecyclerViewAdapter

    @ProvidePresenter
    fun provide() = component().mainComponentBuilder()
            .build()
            .presenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        refresh.setOnRefreshListener { presenter.update() }

        val array = SparseArrayCompat<AdapterViewCreator<AdapterItem<Any>, RecyclerView.ViewHolder>>()
        array.put(ProgressAdapterItem.VIEW_TYPE, ProgressItemCreator() as AdapterViewCreator<AdapterItem<Any>, RecyclerView.ViewHolder>)
        val imageAdapterCreator = ImageAdapterCreator()
        imageAdapterCreator.imageListener = object : ImageAdapterCreator.ImageClickListener {
            override fun onImageClick(imageInfo: ImageInfo) {
                presenter.onItemClick(imageInfo)
            }
        }
        array.put(ImageAdapterItem.VIEW_TYPE, imageAdapterCreator as AdapterViewCreator<AdapterItem<Any>, RecyclerView.ViewHolder>)

        adapter = AppRecyclerViewAdapter(this, layoutInflater, array)

        recyclerView.adapter = adapter
        val manager = GridLayoutManager(this, 2)
        manager.spanSizeLookup = ImageLookUp()
        recyclerView.layoutManager = manager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (manager.findLastCompletelyVisibleItemPosition() == adapter.itemCount - 1) {
                    presenter.loadMore()
                }
            }
        })
    }

    override fun showImages(items: List<AdapterItem<Any>>) {
        adapter.submitList(items)
    }

    override fun openDetailsInfo(imageInfo: ImageInfo) {
        startActivity(ImageDetailsActivity.createIntent(this, imageInfo))
    }

    override fun hideProgress() {
        refresh.isRefreshing = false
    }

    inner class ImageLookUp : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(postition: Int): Int {

            val itemViewType = adapter.getItemViewType(postition)

            return when (itemViewType) {
                ProgressAdapterItem.VIEW_TYPE -> COLUMNS
                else -> 1
            }
        }
    }
}

package com.temq.giphytest.ui.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.design.button.MaterialButton
import android.support.v4.widget.CircularProgressDrawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.temq.giphytest.R
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.ui.BaseActivity
import com.temq.giphytest.utils.GlideApp


class ImageDetailsActivity : BaseActivity(), ImageDetailsView {

    companion object {

        private const val IMAGE_INFO = "IMAGE_INFO"

        @JvmStatic
        fun createIntent(context: Context,
                         imageInfo: ImageInfo): Intent {
            val intent = Intent(context, ImageDetailsActivity::class.java)
            intent.putExtra(IMAGE_INFO, imageInfo)
            return intent
        }
    }

    @BindView(R.id.image)
    lateinit var image: ImageView
    @BindView(R.id.imageName)
    lateinit var titleView: TextView
    @BindView(R.id.userAvatarView)
    lateinit var avatar: ImageView
    @BindView(R.id.userName)
    lateinit var userNameView: TextView
    @BindView(R.id.userNick)
    lateinit var nicknameView: TextView
    @BindView(R.id.gotoGiphy)
    lateinit var gotoGiphyView: MaterialButton
    @BindView(R.id.gotoTwitter)
    lateinit var gotoTwitterView: MaterialButton

    @InjectPresenter
    lateinit var presenter: ImageDetailsPresenter

    @ProvidePresenter
    fun provide(): ImageDetailsPresenter {

        val builder = component().imageDetailsBuilder()
        if (intent.hasExtra(IMAGE_INFO)) {
            val imageInfo = intent.getParcelableExtra<ImageInfo>(IMAGE_INFO)
            builder.imageInfo(imageInfo)
                    .imageId(imageInfo.id)
        } else {
            val uri = intent.data
            val id = uri?.lastPathSegment?.split("-")?.last().orEmpty()
            builder.imageId(id)
        }
        return builder.build().presenter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        ButterKnife.bind(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            val uri = intent.data
            val id = uri?.lastPathSegment?.split("-")?.last().orEmpty()
            presenter.reloadGifInfo(id)
        }
    }

    override fun setAuthorInfo(avatarUrl: String, userName: String, displayName: String) {

        userNameView.text = userName
        nicknameView.text = getString(R.string.user_name_pattern, displayName)

        GlideApp.with(this)
                .load(avatarUrl)
                .circleCrop()
                .into(avatar)
    }

    override fun showTwitter(twitter: String) {
        if (twitter.isEmpty()) {
            gotoTwitterView.visibility = View.GONE
        } else {
            gotoTwitterView.visibility = View.VISIBLE
            gotoTwitterView.text = getString(R.string.twitter_pattern, twitter)
        }
    }

    override fun showGiphyProfile(show: Boolean) {
        gotoGiphyView.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun setImage(gifUrl: String) {

        val circularProgressDrawable = CircularProgressDrawable(this)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        GlideApp.with(this)
                .load(gifUrl)
                .placeholder(circularProgressDrawable)
                .centerInside()
                .fitCenter()
                .into(image)

    }

    override fun openChrome(link: String) {
        openLink(Uri.parse(link))
    }

    override fun openTwitter(userName: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.twitter_app_pattern, userName)))
        packageManager.getPackageInfo("com.twitter.android", 0)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        } else {
            val profileLink = Uri.parse(getString(R.string.twitter_profile_link))
            openLink(profileLink)
        }
    }

    private fun openLink(uri: Uri) {
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(this, uri)
    }

    override fun showTitle(title: String) {
        titleView.text = title
    }

    override fun close() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
//        NavUtils.navigateUpFromSameTask(this)
    }

    @OnClick(R.id.gotoTwitter)
    fun onTwitterClick() {
        presenter.twitterClick()
    }

    @OnClick(R.id.gotoGiphy)
    fun onGiphyClick() {
        presenter.giphyClick()
    }
}
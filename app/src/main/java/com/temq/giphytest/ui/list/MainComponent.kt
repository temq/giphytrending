package com.temq.giphytest.ui.list

import dagger.Subcomponent

@Subcomponent
interface MainComponent {
    fun presenter(): MainPresenter

    @Subcomponent.Builder
    interface Builder {
        fun build(): MainComponent
    }
}
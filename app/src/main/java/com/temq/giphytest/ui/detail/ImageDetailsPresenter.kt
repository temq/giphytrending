package com.temq.giphytest.ui.detail

import com.arellomobile.mvp.InjectViewState
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.model.gif
import com.temq.giphytest.network.ImageLoader
import com.temq.giphytest.ui.BasePresenter
import com.temq.giphytest.utils.AppSchedulers
import io.reactivex.Single
import javax.inject.Inject

@InjectViewState
class ImageDetailsPresenter @Inject constructor(private var imageInfo: ImageInfo?,
                                                private var imageId: String,
                                                private val imageLoader: ImageLoader,
                                                private val schedulers: AppSchedulers) : BasePresenter<ImageDetailsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadData()
    }

    fun twitterClick() {
        imageInfo?.let {
            viewState.openTwitter(it.userV1.twitter)
        }
    }

    fun giphyClick() {
        imageInfo?.let {
            viewState.openChrome(it.userV1.profileUrl)
        }
    }

    private fun loadData() {
        val disposable = loadImageInfo()
                .subscribeOn(schedulers.network())
                .observeOn(schedulers.ui())
                .subscribe(
                        {
                            imageInfo = it
                            setupUi(it)
                        },
                        {
                            handleError(it)
                            viewState.close()
                        })
        add(disposable)
    }

    private fun loadImageInfo(): Single<ImageInfo> = Single.defer {
        if (imageInfo == null) {
            imageLoader.loadGif(imageId)
        } else {
            return@defer Single.just(imageInfo)
        }
    }

    private fun setupUi(imageInfo: ImageInfo) {
        imageInfo.gif()?.apply {
            viewState.setImage(url)
        }
        viewState.showTitle(imageInfo.title)
        viewState.setAuthorInfo(imageInfo.userV1.avatarUrl,
                imageInfo.userV1.displayName,
                imageInfo.userV1.userName)
        viewState.showTwitter(imageInfo.userV1.twitter)
        viewState.showGiphyProfile(imageInfo.userV1.profileUrl.isNotEmpty())
    }

    fun reloadGifInfo(id: String) {
        imageId = id
        imageInfo = null
        loadData()
    }
}
package com.temq.giphytest.ui.list

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.ui.BaseView
import com.temq.giphytest.ui.adapter.AdapterItem

interface MainActivityView : BaseView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showImages(items: List<AdapterItem<Any>>)

    @StateStrategyType(SkipStrategy::class)
    fun openDetailsInfo(imageInfo: ImageInfo)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun hideProgress()
}
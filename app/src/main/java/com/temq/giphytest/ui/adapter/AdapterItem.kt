package com.temq.giphytest.ui.adapter

interface AdapterItem<out T> {
    fun getViewType(): Int

    fun getId(): Long

    fun getItem(): T

    fun areItemSame(adapterItem: AdapterItem<Any>): Boolean

    fun areContentsSame(adapterItem: AdapterItem<Any>): Boolean

    fun getPayload(adapterItem: AdapterItem<Any>): Any? = null
}
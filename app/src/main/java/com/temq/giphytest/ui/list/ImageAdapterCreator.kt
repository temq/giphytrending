package com.temq.giphytest.ui.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.model.bestImage
import com.temq.giphytest.ui.adapter.AdapterViewCreator
import com.temq.giphytest.utils.GlideApp

class ImageAdapterCreator : AdapterViewCreator<ImageAdapterItem, ImageAdapterCreator.ViewHolder> {

    companion object {
        const val IMAGE_WIDTH = 400
        const val IMAGE_HEIGHT = 300
    }

    var imageListener: ImageClickListener? = null

    override fun onCreateViewHolder(layoutInflater: LayoutInflater, viewGroup: ViewGroup): ViewHolder {
        val view = layoutInflater.inflate(ImageAdapterItem.VIEW_TYPE, viewGroup, false)
        return ViewHolder(view)
    }

    override fun bind(viewHolder: ViewHolder, item: ImageAdapterItem, context: Context, payloads: MutableList<Any>) {
        viewHolder.bind(item.getItem())
    }

    interface ImageClickListener {
        fun onImageClick(imageInfo: ImageInfo)
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private val image: ImageView = view as ImageView
        private var info: ImageInfo? = null

        init {
            image.setOnClickListener {
                imageListener?.apply {
                    info?.let { inf -> onImageClick(inf) }
                }
            }
        }

        fun bind(imageInfo: ImageInfo) {
            info = imageInfo

            val imageData = imageInfo.bestImage()

            imageData?.apply {
                GlideApp.with(image)
                        .load(imageData.url)
                        .centerCrop()
                        .override(IMAGE_WIDTH, IMAGE_HEIGHT)
                        .into(image)
            }
        }
    }
}
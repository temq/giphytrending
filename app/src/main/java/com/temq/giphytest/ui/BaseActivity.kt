package com.temq.giphytest.ui

import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.temq.giphytest.App
import com.temq.giphytest.R

abstract class BaseActivity : MvpAppCompatActivity(), BaseView {

    fun component() = (application as App).appComponent

    override fun showError(message: String) {
        showToast(message)
    }

    override fun showUnknownError() {
        showToast(getString(R.string.unknown_error))
    }

    override fun noConnection() {
        showToast(getString(R.string.no_network_connection))
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
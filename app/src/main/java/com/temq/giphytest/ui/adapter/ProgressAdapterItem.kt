package com.temq.giphytest.ui.adapter

import android.support.v7.widget.RecyclerView
import com.temq.giphytest.R

class ProgressAdapterItem : AdapterItem<Any> {

    companion object {
        const val VIEW_TYPE = R.layout.item_progress
    }

    private val any = Any()

    override fun getViewType() = VIEW_TYPE

    override fun getId() = RecyclerView.NO_ID

    override fun getItem() = any

    override fun areItemSame(adapterItem: AdapterItem<Any>) = adapterItem is ProgressAdapterItem

    override fun areContentsSame(adapterItem: AdapterItem<Any>) = true

}
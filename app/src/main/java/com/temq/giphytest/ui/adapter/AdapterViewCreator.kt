package com.temq.giphytest.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

interface AdapterViewCreator<in I : AdapterItem<Any>, VH : RecyclerView.ViewHolder> {
    fun onCreateViewHolder(layoutInflater: LayoutInflater,
                           viewGroup: ViewGroup): VH

    fun bind(viewHolder: VH, item: I, context: Context, payloads: MutableList<Any> = mutableListOf())
}
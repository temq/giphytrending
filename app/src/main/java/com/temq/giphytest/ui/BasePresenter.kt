package com.temq.giphytest.ui

import com.arellomobile.mvp.MvpPresenter
import com.temq.giphytest.BuildConfig
import com.temq.giphytest.utils.ServerException
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.net.UnknownHostException

abstract class BasePresenter<V : BaseView> : MvpPresenter<V>() {
    private val disposables = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
    }

    protected fun add(disposable: Disposable) {
        disposables.add(disposable)
    }

    protected fun handleError(throwable: Throwable): Boolean {

        if (BuildConfig.DEBUG) {
            throwable.printStackTrace()
        }

        return when (throwable) {
            is UnknownHostException -> {
                viewState.noConnection()
                return true
            }

            is HttpException -> {
                showError(throwable)
                return true
            }
            is ServerException -> {
                showError(throwable)
                return true
            }
            else -> false
        }
    }

    private fun showError(throwable: Throwable) {
        throwable.message?.let {
            viewState.showError(it)
        }
    }
}
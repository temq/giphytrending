package com.temq.giphytest.ui.list

import com.arellomobile.mvp.InjectViewState
import com.temq.giphytest.model.ImageInfo
import com.temq.giphytest.network.ImageLoader
import com.temq.giphytest.ui.BasePresenter
import com.temq.giphytest.ui.adapter.AdapterItem
import com.temq.giphytest.ui.adapter.ProgressAdapterItem
import com.temq.giphytest.utils.AppSchedulers
import com.temq.giphytest.utils.OffsetManager
import io.reactivex.disposables.Disposables
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@InjectViewState
class MainPresenter @Inject constructor(private val loader: ImageLoader,
                                        private val appSchedulers: AppSchedulers)
    : BasePresenter<MainActivityView>() {

    private val offsetManager = OffsetManager(24)
    private var currentItems: List<AdapterItem<Any>> = Collections.emptyList()
    private var loadingDisposable = Disposables.disposed()

    private val progressItem by lazy { ProgressAdapterItem() }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        update()
    }

    fun update() {
        offsetManager.reset()
        currentItems = Collections.emptyList()
        loadMore()
    }

    fun loadMore() {

        if (!loadingDisposable.isDisposed || !offsetManager.canLoadModer) {
            return
        }

        loadingDisposable = loader.loadGifs(offsetManager.offset, offsetManager.limit)
                .doOnSuccess { offsetManager.itemsLoaded(it.size) }
                .map { convertToAdapterItem(it) }
                .map {

                    val size = currentItems.size + it.size + if (offsetManager.canLoadModer) 1 else 0
                    val list = ArrayList<AdapterItem<Any>>(size)
                    val progress = currentItems.getOrElse(currentItems.lastIndex) { progressItem }
                    if (!currentItems.isEmpty()) {
                        list.addAll(currentItems.subList(0, currentItems.lastIndex)) // without progress item
                    }
                    list.addAll(it)
                    if (offsetManager.canLoadModer) {
                        if (progress is ProgressAdapterItem) {
                            list.add(progress)
                        } else {
                            list.add(progressItem)
                        }
                    }

                    return@map list
                }
                .subscribeOn(appSchedulers.network())
                .observeOn(appSchedulers.ui())
                .doOnSuccess { currentItems = it }
                .subscribe(
                        {
                            viewState.hideProgress()
                            viewState.showImages(it)
                        },
                        {
                            viewState.hideProgress()
                            handleError(it)
                        })
    }

    private fun convertToAdapterItem(items: List<ImageInfo>): List<AdapterItem<Any>> {
        return items.map {
            return@map ImageAdapterItem(it)
        }
    }

    fun onItemClick(imageInfo: ImageInfo) {
        viewState.openDetailsInfo(imageInfo)
    }
}
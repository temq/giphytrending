package com.temq.giphytest.dagger

import com.temq.giphytest.App
import com.temq.giphytest.ui.detail.ImageDetailsComponent
import com.temq.giphytest.ui.list.MainComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [LoaderModule::class,
    NetworkModule::class])
@Singleton
interface AppComponent {

    fun mainComponentBuilder(): MainComponent.Builder

    fun imageDetailsBuilder(): ImageDetailsComponent.Builder

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(app: App): Builder

        fun build(): AppComponent
    }

}
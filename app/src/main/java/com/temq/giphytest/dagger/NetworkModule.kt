package com.temq.giphytest.dagger

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.temq.giphytest.network.v1.GifClientV1
import com.temq.giphytest.network.v1.ImageTypeSrializer
import com.temq.giphytest.network.v1.ImageTypeV1
import com.temq.giphytest.utils.ApiErrorInterceptor
import com.temq.giphytest.utils.ApiKeyInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
object NetworkModule {
    @Provides
    @JvmStatic
    fun revolutGson(): Gson = GsonBuilder()
            .registerTypeAdapter(ImageTypeV1::class.java, ImageTypeSrializer())
            .create()

    @Provides
    @JvmStatic
    fun logInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    @Provides
    @JvmStatic
    fun client(logInterceptor: HttpLoggingInterceptor,
               errorInterceptor: ApiErrorInterceptor): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(ApiKeyInterceptor())
            .addInterceptor(errorInterceptor)
            .addInterceptor(logInterceptor)
            .build()

    @Provides
    @JvmStatic
    @Singleton
    fun countryRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
            .baseUrl("https://api.giphy.com/")
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    @Provides
    @JvmStatic
    @Singleton
    fun gifClient(retrofit: Retrofit) = retrofit.create(GifClientV1::class.java)
}
package com.temq.giphytest.dagger

import com.temq.giphytest.network.ImageLoader
import com.temq.giphytest.network.v1.NetworkImageLoader
import dagger.Binds
import dagger.Module

@Module
interface LoaderModule {

    @Binds
    fun imageLoader(loader: NetworkImageLoader): ImageLoader
}
package com.temq.giphytest.utils

import okhttp3.Interceptor
import okhttp3.Response


class ApiKeyInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val url = request.url().newBuilder()
                .addQueryParameter("api_key", "T0X7QMH8Srd0fta8bfxwjTQRGpZrizM1")
                .build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}
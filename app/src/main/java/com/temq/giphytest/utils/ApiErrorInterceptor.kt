package com.temq.giphytest.utils

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import okhttp3.Interceptor
import okhttp3.Response
import java.nio.charset.Charset
import javax.inject.Inject

class ApiErrorInterceptor @Inject constructor(private val gson: Gson) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        val body = response.body()

        if (body != null && !response.isSuccessful
                && response.code() >= 400 && response.code() < 500) {
            val source = body.source()
            source.request(Long.MAX_VALUE) // Buffer the entire body.
            val buffer = source.buffer()
            val charset = body.contentType()!!.charset(Charset.forName("UTF-8"))
            // Clone the existing buffer is they can only read once so we still want to pass the original one to the chain.
            val json = buffer.clone().readString(charset!!)
            val serverException = try {
                val data = gson.fromJson<DataHolder>(json, DataHolder::class.java)
                ServerException(data.meta.error, data.meta.code)
            } catch (exception: Exception) {
                try {
                    val error = gson.fromJson<Error>(json, Error::class.java)
                    ServerException(error.message, response.code())
                } catch (throwable: Throwable) {
                    throwable
                }
            }
            throw serverException
        }

        return response
    }
}

data class DataHolder(@SerializedName("meta") val meta: Meta)

data class Meta(@SerializedName("status") val code: Int,
                @SerializedName("msg") val error: String)

data class Error(@SerializedName("message") val message: String);

class ServerException(val error: String, val code: Int = 0) : Exception(error)
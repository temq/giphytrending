package com.temq.giphytest.utils

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

class OffsetManager(val limit: Int) {
    private val _counter = AtomicInteger()
    val offset: Int
        get() = _counter.get()

    private val _canLoadModer = AtomicBoolean(true)
    val canLoadModer: Boolean
        get() = _canLoadModer.get()

    fun itemsLoaded(count: Int) {
        _counter.addAndGet(count)
        _canLoadModer.set(count >= limit)
    }

    fun reset() {
        _counter.set(0)
        _canLoadModer.set(true)
    }
}
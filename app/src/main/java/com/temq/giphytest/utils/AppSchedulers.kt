package com.temq.giphytest.utils

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppSchedulers @Inject constructor() {

    fun ui() = AndroidSchedulers.mainThread()

    fun network() = Schedulers.io()

    fun background() = Schedulers.computation()
}
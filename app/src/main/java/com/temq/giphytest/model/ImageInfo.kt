package com.temq.giphytest.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

enum class ImageType {
    GIF
}

@Parcelize
data class User(val avatarUrl: String = "",
                val profileUrl: String = "",
                val userName: String = "",
                val displayName: String = "",
                val bannerUrl: String = "",
                val twitter: String = "") : Parcelable

@Parcelize
data class Image(val url: String,
                 val width: Int,
                 val height: Int,
                 val size: Int = 0) : Parcelable

@Parcelize
data class ImageInfo(val typeV1: ImageType,
                     val id: String,
                     val title: String,
                     val userName: String,
                     val userV1: User,
                     val images: Map<String, Image>) : Parcelable


fun ImageInfo.bestImage(): Image? {
    return images["480w_still"]
}

fun ImageInfo.gif(): Image? {
    if (images.containsKey("original")) {
        return images["original"]
    }
    return bestImage()
}
